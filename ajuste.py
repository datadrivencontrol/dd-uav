#!/Users/diego/anaconda3/bin/python

import pyulog
import matplotlib.pyplot as plt
import sys
from scipy import signal
import numpy as np
import vrft
import sysid
import glob
import os
import math


# Escolhe arquivo
if (len(sys.argv)>1):
    arq=sys.argv[1]
else:
    list_of_files=glob.glob('*.ulg')
    arq = max(list_of_files, key=os.path.getctime)
    print(arq)

# Carrega Mensagens
msg=['vehicle_attitude','vehicle_attitude_setpoint','actuator_controls_0','vehicle_rates_setpoint'];

ulog = pyulog.ULog(arq, msg)

for d in ulog.data_list:

    if d.name=='vehicle_rates_setpoint':
        t0=d.data['timestamp']
        r_r_r=d.data['roll']
        r_r_p=d.data['pitch']
        r_r_y=d.data['yaw']


    if d.name=='vehicle_attitude':
        t1=d.data['timestamp']
        y_r_r=d.data['rollspeed']
        y_r_p=d.data['pitchspeed']
        y_r_y=d.data['yawspeed']
        q0=d.data['q[0]']
        q1=d.data['q[1]']
        q2=d.data['q[2]']
        q3=d.data['q[3]']

    if d.name=='actuator_controls_0':
        t2=d.data['timestamp']
        u_r=d.data['control[0]']
        u_p=d.data['control[1]']
        u_y=d.data['control[2]']
        u_z=d.data['control[3]']
        
    if d.name=='vehicle_attitude_setpoint':
        t3=d.data['timestamp']
        q0d=d.data['q_d[0]']
        q1d=d.data['q_d[1]']
        q2d=d.data['q_d[2]']
        q3d=d.data['q_d[3]']
  
N=np.size(q0)
y_a_p=np.zeros(N)
y_a_r=np.zeros(N)
for i in range(0, N):
    y_a_p[i] = -math.asin(2*q1[i]*q3[i]-2*q2[i]*q0[i])
    tt0 = +2.0 * (q0[i] * q1[i] + q2[i] * q3[i])
    tt1 = +1.0 - 2.0 * (q1[i] * q1[i] + q2[i] * q2[i])
    y_a_r[i] = math.atan2(tt0, tt1)

N=np.size(q0d)
r_a_p=np.zeros(N)
r_a_r=np.zeros(N)
for i in range(0, N):
    r_a_p[i] = -math.asin(2*q1d[i]*q3d[i]-2*q2d[i]*q0d[i])
    tt0 = +2.0 * (q0d[i] * q1d[i] + q2d[i] * q3d[i])
    tt1 = +1.0 - 2.0 * (q1d[i] * q1d[i] + q2d[i] * q2d[i])
    r_a_r[i] = math.atan2(tt0, tt1)


# Coloca em segundos
t0=t0*1e-6
t1=t1*1e-6;
t2=t2*1e-6;
t3=t3*1e-6;

# Número de amostras
N=np.size(t0)
P=N/(t0[N-1]-t0[0])
print(P)

# Interpola os dados para todos terem a mesma base de tempo
y_r_r=np.interp(t0, t1, y_r_r)
y_r_p=np.interp(t0, t1, y_r_p)
y_r_y=np.interp(t0, t1, y_r_y)

u_r=np.interp(t0, t2, u_r)
u_p=np.interp(t0, t2, u_p)
u_y=np.interp(t0, t2, u_y)
u_z=np.interp(t0, t2, u_z)


y_a_r=np.interp(t0, t1, y_a_r)
y_a_p=np.interp(t0, t1, y_a_p)

r_a_r=np.interp(t0, t3, r_a_r)
r_a_p=np.interp(t0, t3, r_a_p)

print("\n=== ROLL RATE")

a=0.98
#b0=np.array([0, 1-a])
#a0=np.array([1, -a])
b0=np.array([0, 4*(1-a), -4*(1-a)*a])
a0=np.array([1, -4*a+2, (2*a-1)*(2*a-1)])

yd_r_r = signal.lfilter(b0, a0, r_r_r)
e_r_r=yd_r_r-y_r_r
e_r_r=e_r_r[2500:N-2500]

J_r_r=math.sqrt(np.dot(e_r_r.T,e_r_r)/N)
print("Custo: ", J_r_r)

#rho_vrft=vrft.vrft_pid_mf(u_r, y_r_r, a0, b0, 'pi'    ,P,0.98)
rho_vrft=vrft.vrft_pid_mf(u_r, y_r_r, a0, b0, 'loco'  ,P,0.98)



print("\n=== PITCH RATE")
a=0.98
#b0=np.array([0, 1-a])
#a0=np.array([1, -a])
b0=np.array([0, 4*(1-a), -4*(1-a)*a])
a0=np.array([1, -4*a+2, (2*a-1)*(2*a-1)])

yd_r_p = signal.lfilter(b0, a0, r_r_p)
e_r_p=yd_r_p-y_r_p
e_r_p=e_r_p[2500:N-2500]
J_r_p=math.sqrt(np.dot(e_r_p.T,e_r_p)/N)
print("Custo: ", J_r_p)
#rho_vrft=vrft.vrft_pid_mf(u_p, y_r_p, a0, b0, 'pi'    ,P,0.98)
rho_vrft=vrft.vrft_pid_mf(u_p, y_r_p, a0, b0, 'loco'  ,P,0.98)



print("\n=== YAW RATE")

a=0.99
#b0=np.array([0, 1-a])
#a0=np.array([1, -a])
b0=np.array([0, 4*(1-a), -4*(1-a)*a])
a0=np.array([1, -4*a+2, (2*a-1)*(2*a-1)])

yd_r_y = signal.lfilter(b0, a0, r_r_y)

e_r_y=yd_r_y-y_r_y
e_r_y=e_r_y[2500:N-2500]
J_r_y=math.sqrt(np.dot(e_r_y.T,e_r_y)/N)
print("Custo: ",J_r_y)

#rho_vrft=vrft.vrft_pid_mf(u_y, y_r_y, a0, b0, 'pi'    ,P,0.99)
rho_vrft=vrft.vrft_pid_mf(u_y, y_r_y, a0, b0, 'loco'  ,P,0.99)

print("\n=== ROLL ANGLE")

a=0.99
b0=np.array([0, 1-a])
a0=np.array([1, -a])
#b0=np.array([0, 4*(1-a), -4*(1-a)*a])
#a0=np.array([1, -4*a+2, (2*a-1)*(2*a-1)])

yd_a_r = signal.lfilter(b0, a0, r_a_r)
e_a_r=yd_a_r-y_a_r
e_a_r=e_r_r[2500:N-2500]

J_a_r=math.sqrt(np.dot(e_a_r.T,e_a_r)/N)
print("Custo: ", J_a_r)

rho_vrft=vrft.vrft_pid_mf(r_r_r, y_a_r, a0, b0, 'p'    ,P,1)

print("\n=== PITCH ANGLE")

a=0.99
b0=np.array([0, 1-a])
a0=np.array([1, -a])
#b0=np.array([0, 4*(1-a), -4*(1-a)*a])
#a0=np.array([1, -4*a+2, (2*a-1)*(2*a-1)])

yd_a_p = signal.lfilter(b0, a0, r_a_p)
e_a_p=yd_a_p-y_a_p
e_a_p=e_r_p[2500:N-2500]

J_a_p=math.sqrt(np.dot(e_a_p.T,e_a_p)/N)
print("Custo: ", J_a_p)

rho_vrft=vrft.vrft_pid_mf(r_r_p, y_a_p, a0, b0, 'p'    ,P,1)


# Plota os graficos

K=180/math.pi


p1=plt.figure(1)
plt.plot(t0,u_r, label='roll')
plt.plot(t0,u_p, label='pitch')
plt.plot(t0,u_y, label='yaw')
plt.plot(t0,u_z, label='z')
plt.ylabel('u')
plt.legend()

p2=plt.figure(2)
plt.plot(t0,r_r_r*K, label='r')
plt.plot(t0,yd_r_r*K, label='yd')
plt.plot(t0,y_r_r*K, label='y')
plt.ylabel('roll speed')
plt.legend()

p3=plt.figure(3)
plt.plot(t0,r_r_p*K, label='r')
plt.plot(t0,yd_r_p*K, label='yd')
plt.plot(t0,y_r_p*K, label='y')
plt.ylabel('pitch speed')
plt.legend()

p4=plt.figure(4)
plt.plot(t0,r_r_y*K, label='r')
plt.plot(t0,yd_r_y*K, label='yd')
plt.plot(t0,y_r_y*K, label='y')
plt.ylabel('yaw speed')
plt.legend()

p5=plt.figure(5)
plt.plot(t0,r_a_r*K, label='r')
plt.plot(t0,yd_a_r*K, label='yd')
plt.plot(t0,y_a_r*K, label='y')
plt.ylabel('roll angle')
plt.legend()

p6=plt.figure(6)
plt.plot(t0,r_a_p*K, label='r')
plt.plot(t0,yd_a_p*K, label='yd')
plt.plot(t0,y_a_p*K, label='y')
plt.ylabel('pitch angle')
plt.legend()


plt.show()






